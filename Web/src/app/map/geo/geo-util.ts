import { Point } from './point';
import { PointEN } from './point-en';
import { PointENObj } from './point-en-obj';

export class GeoUtil {

    private static x_PI: number = 3.14159265358979324 * 3000.0 / 180.0;
    private static PI: number = Math.PI;
    private static a: number = 6378245.0;
    private static ee: number = 0.00669342162296594323;

    public static EARTH_RADIUS: number = GeoUtil.a;

    /**
     * js小数取整方法（去掉小数点，不四舍五入）：
     * 1、~~1.8
     * 2、1.8>>0
     * 3、Math.floor(1.8)
     */

    public static getCirclePoints(center: Point, radius: number): Array<Point> {
        let num = 72;
        let cphase = 2 * Math.PI / num;
        let pts = [];
        for (let k = 0; k < num; k++) {
            let dx = (radius * Math.cos(k * cphase));
            let dy = (radius * Math.sin(k * cphase));
            let dlng = dx / (GeoUtil.EARTH_RADIUS * Math.cos(center.lat * Math.PI / 180) * Math.PI / 180);
            let dlat = dy / (GeoUtil.EARTH_RADIUS * Math.PI / 180);
            let newlng = center.lng + dlng;
            let newlat = center.lat + dlat;
            pts.push(new Point(newlng, newlat));
        }
        return pts;
    }

    public static lnglat2ENPoint(point: Point): PointEN {
        let e_h1: number = ~~point.lng;
        let e_m1: number = ~~((point.lng - e_h1) * 60);
        let e_s1: string = ((((point.lng - e_h1) * 60) - e_m1) * 60).toFixed(1);
        let e: string = "E" + e_h1 + "°" + e_m1 + "′" + e_s1 + "″";
        let n_h1: number = ~~point.lat;
        let n_m1: number = ~~((point.lat - n_h1) * 60);
        let n_s1: string = ((((point.lat - n_h1) * 60) - n_m1) * 60).toFixed(1);
        let n: string = "N" + n_h1 + "°" + n_m1 + "′" + n_s1 + "″";
        return new PointEN(e, n);
    }

    public static lnglat2ENPointFix0(point: Point): PointEN {
        let e_h1: number = ~~point.lng;
        let e_m1: number = ~~((point.lng - e_h1) * 60);
        let e_m1_fix0: string;
        if (e_m1 < 10) {
            e_m1_fix0 = "0" + e_m1;
        } else {
            e_m1_fix0 = "" + e_m1;
        }
        let e_s1: number = (((point.lng - e_h1) * 60) - e_m1) * 60;
        let e_s1_fix0: string;
        if (e_s1 < 10) {
            e_s1_fix0 = "0" + e_s1.toFixed(1);
        } else {
            e_s1_fix0 = e_s1.toFixed(1);
        }
        let e: string = "E" + e_h1 + "°" + e_m1_fix0 + "′" + e_s1_fix0 + "″";
        let n_h1: number = ~~point.lat;
        let n_m1: number = ~~((point.lat - n_h1) * 60);
        let n_m1_fix0: string;
        if (n_m1 < 10) {
            n_m1_fix0 = "0" + n_m1;
        } else {
            n_m1_fix0 = "" + n_m1;
        }
        let n_s1: number = (((point.lat - n_h1) * 60) - n_m1) * 60;
        let n_s1_fix0: string;
        if (n_s1 < 10) {
            n_s1_fix0 = "0" + n_s1.toFixed(1);
        } else {
            n_s1_fix0 = n_s1.toFixed(1);
        }
        let n: string = "N" + n_h1 + "°" + n_m1_fix0 + "′" + n_s1_fix0 + "″";
        return new PointEN(e, n);
    }

    public static lnglat2ENPointObj(point: Point): PointENObj {
        let e_h1: number = ~~point.lng;
        let e_m1: number = ~~((point.lng - e_h1) * 60);
        let e_s1: string = ((((point.lng - e_h1) * 60) - e_m1) * 60).toFixed(1);
        let n_h1: number = ~~point.lat;
        let n_m1: number = ~~((point.lat - n_h1) * 60);
        let n_s1: string = ((((point.lat - n_h1) * 60) - n_m1) * 60).toFixed(1);
        return new PointENObj(
            { h: e_h1, m: e_m1, s: Number(e_s1) },
            { h: n_h1, m: n_m1, s: Number(n_s1) }
        );
    }

    public static ENPoint2lnglat(pointEN: PointEN) {
        let lng: string = pointEN.lng.split("E")[1];
        let h_lng: number = parseInt(lng.split("°")[0]);
        let m_lng: number = parseFloat(lng.split("°")[1].split("′")[0]) / 60;
        let s_lng: number = parseFloat(lng.split("°")[1].split("′")[1].split("″")[0]) / 3600;
        let out_lng: number = parseFloat((h_lng + m_lng + s_lng).toFixed(6));
        let lat: string = pointEN.lat.split("N")[1];
        let h_lat: number = parseInt(lat.split("°")[0]);
        let m_lat: number = parseFloat(lat.split("°")[1].split("′")[0]) / 60;
        let s_lat: number = parseFloat(lat.split("°")[1].split("′")[1].split("″")[0]) / 3600;
        let out_lat: number = parseFloat((h_lat + m_lat + s_lat).toFixed(6));
        return new Point(out_lng, out_lat);
    }

    public static LngLat_To_Mercator(point: Point): Point {
        let x = point.lng * 20037508.34 / 180;
        let y = Math.log(Math.tan((90 + point.lat) * Math.PI / 360)) / (Math.PI / 180);
        y = y * 20037508.34 / 180;
        return new Point(x, y);
    }

    public static Mercator_To_LngLat(point: Point): Point {
        let x = point.lng / 20037508.34 * 180;
        let y = point.lat / 20037508.34 * 180;
        y = 180 / Math.PI * (2 * Math.atan(Math.exp(y * Math.PI / 180)) - Math.PI / 2);
        return new Point(x, y);
    }

    public static wgs84_To_gcj02(point: Point): Point {
        let lat = +point.lat;
        let lng = +point.lng;
        if (this.outOfChina(point)) {
            return point;
        } else {
            let dlat = this.transformLat(new Point(lng - 105.0, lat - 35.0));
            let dlng = this.transformLng(new Point(lng - 105.0, lat - 35.0));
            let radlat = lat / 180.0 * this.PI;
            let magic = Math.sin(radlat);
            magic = 1 - this.ee * magic * magic;
            let sqrtmagic = Math.sqrt(magic);
            dlat = (dlat * 180.0) / ((this.a * (1 - this.ee)) / (magic * sqrtmagic) * this.PI);
            dlng = (dlng * 180.0) / (this.a / sqrtmagic * Math.cos(radlat) * this.PI);
            let mglat = lat + dlat;
            let mglng = lng + dlng;
            return new Point(mglng, mglat);
        }
    }

    public static gcj02_To_wgs84(point: Point): Point {
        let lat = +point.lat;
        let lng = +point.lng;
        if (this.outOfChina(point)) {
            return point;
        } else {
            let dlat = this.transformLat(new Point(lng - 105.0, lat - 35.0));
            let dlng = this.transformLng(new Point(lng - 105.0, lat - 35.0));
            let radlat = lat / 180.0 * this.PI;
            let magic = Math.sin(radlat);
            magic = 1 - this.ee * magic * magic;
            let sqrtmagic = Math.sqrt(magic);
            dlat = (dlat * 180.0) / ((this.a * (1 - this.ee)) / (magic * sqrtmagic) * this.PI);
            dlng = (dlng * 180.0) / (this.a / sqrtmagic * Math.cos(radlat) * this.PI);
            let mglat = lat + dlat;
            let mglng = lng + dlng;
            return new Point(lng * 2 - mglng, lat * 2 - mglat);
        }
    }

    private static transformLat(point: Point): number {
        let lat = +point.lat;
        let lng = +point.lng;
        let ret = -100.0 + 2.0 * lng + 3.0 * lat + 0.2 * lat * lat + 0.1 * lng * lat + 0.2 * Math.sqrt(Math.abs(lng));
        ret += (20.0 * Math.sin(6.0 * lng * this.PI) + 20.0 * Math.sin(2.0 * lng * this.PI)) * 2.0 / 3.0;
        ret += (20.0 * Math.sin(lat * this.PI) + 40.0 * Math.sin(lat / 3.0 * this.PI)) * 2.0 / 3.0;
        ret += (160.0 * Math.sin(lat / 12.0 * this.PI) + 320 * Math.sin(lat * this.PI / 30.0)) * 2.0 / 3.0;
        return ret;
    }

    private static transformLng(point: Point): number {
        let lat = +point.lat;
        let lng = +point.lng;
        let ret = 300.0 + lng + 2.0 * lat + 0.1 * lng * lng + 0.1 * lng * lat + 0.1 * Math.sqrt(Math.abs(lng));
        ret += (20.0 * Math.sin(6.0 * lng * this.PI) + 20.0 * Math.sin(2.0 * lng * this.PI)) * 2.0 / 3.0;
        ret += (20.0 * Math.sin(lng * this.PI) + 40.0 * Math.sin(lng / 3.0 * this.PI)) * 2.0 / 3.0;
        ret += (150.0 * Math.sin(lng / 12.0 * this.PI) + 300.0 * Math.sin(lng / 30.0 * this.PI)) * 2.0 / 3.0;
        return ret;
    }

    private static outOfChina(point: Point): boolean {
        let lat = +point.lat;
        let lng = +point.lng;
        // 纬度3.86~53.55,经度73.66~135.05
        return !(lng > 73.66 && lng < 135.05 && lat > 3.86 && lat < 53.55);
    }

    /** 两点之间的距离 */
    public static getDistance(point1: Point, point2: Point): number {
        point1.lng = this.getLoop(point1.lng, -180, 180);
        point1.lat = this.getRange(point1.lat, -74, 74);
        point2.lng = this.getLoop(point2.lng, -180, 180);
        point2.lat = this.getRange(point2.lat, -74, 74);
        let x1, x2, y1, y2;
        x1 = this.degToRad(point1.lng);
        y1 = this.degToRad(point1.lat);
        x2 = this.degToRad(point2.lng);
        y2 = this.degToRad(point2.lat);
        return this.a * Math.acos((Math.sin(y1) * Math.sin(y2) + Math.cos(y1) * Math.cos(y2) * Math.cos(x2 - x1)));
    }

    /** 将度转化为弧度 */
    public static degToRad(deg: number): number {
        return Math.PI * deg / 180;
    }

    /** 将弧度转化为度 */
    public static radToDeg(rad: number): number {
        return rad * 180 / Math.PI;
    }

    /** 将v值限定在a,b之间，纬度使用 */
    private static getRange(v: number, a: number, b: number): number {
        if (a != null) {
            v = Math.max(v, a);
        }
        if (b != null) {
            v = Math.min(v, b);
        }
        return v;
    }

    /** 将v值限定在a,b之间，经度使用 */
    private static getLoop(v: number, a: number, b: number): number {
        while (v > b) {
            v -= b - a;
        }
        while (v < a) {
            v += b - a;
        }
        return v;
    }

    /** 计算多边形面积 */
    public static getPolygonArea(points: Array<Point>): number {
        if (points[0].lng == points[points.length - 1].lng && points[0].lat == points[points.length - 1].lat) {
            points = points.splice(points.length - 1, 1);
        }
        if (points.length < 3) {
            return 0;
        }
        let totalArea = 0;
        let LowX = 0.0;
        let LowY = 0.0;
        let MiddleX = 0.0;
        let MiddleY = 0.0;
        let HighX = 0.0;
        let HighY = 0.0;
        let AM = 0.0;
        let BM = 0.0;
        let CM = 0.0;
        let AL = 0.0;
        let BL = 0.0;
        let CL = 0.0;
        let AH = 0.0;
        let BH = 0.0;
        let CH = 0.0;
        let CoefficientL = 0.0;
        let CoefficientH = 0.0;
        let ALtangent = 0.0;
        let BLtangent = 0.0;
        let CLtangent = 0.0;
        let AHtangent = 0.0;
        let BHtangent = 0.0;
        let CHtangent = 0.0;
        let ANormalLine = 0.0;
        let BNormalLine = 0.0;
        let CNormalLine = 0.0;
        let OrientationValue = 0.0;
        let AngleCos = 0.0;
        let Sum1 = 0.0;
        let Sum2 = 0.0;
        let Count2 = 0;
        let Count1 = 0;
        let Sum = 0.0;
        let Radius = this.EARTH_RADIUS;
        let Count = points.length;
        for (let i = 0; i < Count; i++) {
            if (i == 0) {
                LowX = points[Count - 1].lng * Math.PI / 180;
                LowY = points[Count - 1].lat * Math.PI / 180;
                MiddleX = points[0].lng * Math.PI / 180;
                MiddleY = points[0].lat * Math.PI / 180;
                HighX = points[1].lng * Math.PI / 180;
                HighY = points[1].lat * Math.PI / 180;
            }
            else if (i == Count - 1) {
                LowX = points[Count - 2].lng * Math.PI / 180;
                LowY = points[Count - 2].lat * Math.PI / 180;
                MiddleX = points[Count - 1].lng * Math.PI / 180;
                MiddleY = points[Count - 1].lat * Math.PI / 180;
                HighX = points[0].lng * Math.PI / 180;
                HighY = points[0].lat * Math.PI / 180;
            }
            else {
                LowX = points[i - 1].lng * Math.PI / 180;
                LowY = points[i - 1].lat * Math.PI / 180;
                MiddleX = points[i].lng * Math.PI / 180;
                MiddleY = points[i].lat * Math.PI / 180;
                HighX = points[i + 1].lng * Math.PI / 180;
                HighY = points[i + 1].lat * Math.PI / 180;
            }
            AM = Math.cos(MiddleY) * Math.cos(MiddleX);
            BM = Math.cos(MiddleY) * Math.sin(MiddleX);
            CM = Math.sin(MiddleY);
            AL = Math.cos(LowY) * Math.cos(LowX);
            BL = Math.cos(LowY) * Math.sin(LowX);
            CL = Math.sin(LowY);
            AH = Math.cos(HighY) * Math.cos(HighX);
            BH = Math.cos(HighY) * Math.sin(HighX);
            CH = Math.sin(HighY);
            CoefficientL = (AM * AM + BM * BM + CM * CM) / (AM * AL + BM * BL + CM * CL);
            CoefficientH = (AM * AM + BM * BM + CM * CM) / (AM * AH + BM * BH + CM * CH);
            ALtangent = CoefficientL * AL - AM;
            BLtangent = CoefficientL * BL - BM;
            CLtangent = CoefficientL * CL - CM;
            AHtangent = CoefficientH * AH - AM;
            BHtangent = CoefficientH * BH - BM;
            CHtangent = CoefficientH * CH - CM;
            AngleCos = (AHtangent * ALtangent + BHtangent * BLtangent + CHtangent * CLtangent) / (Math.sqrt(AHtangent * AHtangent + BHtangent * BHtangent + CHtangent * CHtangent) * Math.sqrt(ALtangent * ALtangent + BLtangent * BLtangent + CLtangent * CLtangent));
            AngleCos = Math.acos(AngleCos);
            ANormalLine = BHtangent * CLtangent - CHtangent * BLtangent;
            BNormalLine = 0 - (AHtangent * CLtangent - CHtangent * ALtangent);
            CNormalLine = AHtangent * BLtangent - BHtangent * ALtangent;
            if (AM != 0)
                OrientationValue = ANormalLine / AM;
            else if (BM != 0)
                OrientationValue = BNormalLine / BM;
            else
                OrientationValue = CNormalLine / CM;
            if (OrientationValue > 0) {
                Sum1 += AngleCos;
                Count1++;
            }
            else {
                Sum2 += AngleCos;
                Count2++;
            }
        }
        let tempSum1, tempSum2;
        tempSum1 = Sum1 + (2 * Math.PI * Count2 - Sum2);
        tempSum2 = (2 * Math.PI * Count1 - Sum1) + Sum2;
        if (Sum1 > Sum2) {
            if ((tempSum1 - (Count - 2) * Math.PI) < 1)
                Sum = tempSum1;
            else
                Sum = tempSum2;
        }
        else {
            if ((tempSum2 - (Count - 2) * Math.PI) < 1)
                Sum = tempSum2;
            else
                Sum = tempSum1;
        }
        totalArea = (Sum - (Count - 2) * Math.PI) * Radius * Radius;
        return totalArea;
    }

    /** 使用数学的方法计算需要画扇形的圆弧上的点坐标 */
    public static EOffsetBearing(point: Point, dist: number, bearing: number): Point {
        let lngConv = this.getDistance(point, new Point(point.lng + 0.1, point.lat)) * 10; //计算1经度与原点的距离
        let latConv = this.getDistance(point, new Point(point.lng, point.lat + 0.1)) * 10; //计算1纬度与原点的距离
        let lat = dist * Math.sin(bearing * Math.PI / 180) / latConv; //正弦计算待获取的点的纬度与原点纬度差
        let lng = dist * Math.cos(bearing * Math.PI / 180) / lngConv; //余弦计算待获取的点的经度与原点经度差
        return new Point(point.lng + lng, point.lat + lat);
    }

    /** 根据一点的经纬度求另一点与其相对的北向夹角 */
    public static getNorthByPointAB(point_a: Point, point_b: Point): number {
        let point_c = new Point(point_a.lng, point_b.lat);
        let ab = this.getDistance(point_a, point_b);
        let bc = this.getDistance(point_b, point_c);
        // 0
        if (point_a.lng == point_b.lng && point_a.lat == point_b.lat) {
            return 0;
        }
        // 0 <= n <= 90
        if (point_a.lng <= point_b.lng && point_a.lat <= point_b.lat) {
            if (bc < ab) {
                let BAC = Math.asin(bc / ab);
                return this.radToDeg(BAC);
            } else {
                return 90;
            }
        }
        // 90 < n <= 180
        if (point_a.lng <= point_b.lng && point_a.lat > point_b.lat) {
            if (bc < ab) {
                let BAC = Math.asin(bc / ab);
                return 180 - this.radToDeg(BAC);
            } else {
                return 90;
            }
        }
        // 180 < n <= 270
        if (point_a.lng >= point_b.lng && point_a.lat >= point_b.lat) {
            if (bc < ab) {
                let BAC = Math.asin(bc / ab);
                return 180 + this.radToDeg(BAC);
            } else {
                return 270;
            }
        }
        // 270 < n <= 360
        if (point_a.lng >= point_b.lng && point_a.lat < point_b.lat) {
            if (bc < ab) {
                let BAC = Math.asin(bc / ab);
                return 360 - this.radToDeg(BAC);
            } else {
                return 270;
            }
        }
        return 0;
    }

    /** 根据某一点与北向夹角和距离，求另一点 */
    public static destinationVincenty(point: Point, northAngle: number, distance: number): Point {
        /* 椭圆球基础参数*/
        let VincentyConstants = {
            a: this.a,
            b: this.EARTH_RADIUS,
            f: 1 / 298.257223563
        };
        let ct = VincentyConstants;
        let a = ct.a, b = ct.b, f = ct.f;

        let lon1 = point.lng;
        let lat1 = point.lat;

        let s = distance;
        let alpha1 = this.degToRad(northAngle);
        let sinAlpha1 = Math.sin(alpha1);
        let cosAlpha1 = Math.cos(alpha1);

        let tanU1 = (1 - f) * Math.tan(this.degToRad(lat1));
        let cosU1 = 1 / Math.sqrt((1 + tanU1 * tanU1)), sinU1 = tanU1 * cosU1;
        let sigma1 = Math.atan2(tanU1, cosAlpha1);
        let sinAlpha = cosU1 * sinAlpha1;
        let cosSqAlpha = 1 - sinAlpha * sinAlpha;
        let uSq = cosSqAlpha * (a * a - b * b) / (b * b);
        let A = 1 + uSq / 16384 * (4096 + uSq * (-768 + uSq * (320 - 175 * uSq)));
        let B = uSq / 1024 * (256 + uSq * (-128 + uSq * (74 - 47 * uSq)));

        let sigma = s / (b * A), sigmaP = 2 * Math.PI;
        let cos2SigmaM = 0, sinSigma = 0, cosSigma = 0;
        while (Math.abs(sigma - sigmaP) > 1e-12) {
            cos2SigmaM = Math.cos(2 * sigma1 + sigma);
            sinSigma = Math.sin(sigma);
            cosSigma = Math.cos(sigma);
            let deltaSigma = B * sinSigma * (cos2SigmaM + B / 4 * (cosSigma * (-1 + 2 * cos2SigmaM * cos2SigmaM) -
                B / 6 * cos2SigmaM * (-3 + 4 * sinSigma * sinSigma) * (-3 + 4 * cos2SigmaM * cos2SigmaM)));
            sigmaP = sigma;
            sigma = s / (b * A) + deltaSigma;
        }

        let tmp = sinU1 * sinSigma - cosU1 * cosSigma * cosAlpha1;
        let lat2 = Math.atan2(sinU1 * cosSigma + cosU1 * sinSigma * cosAlpha1,
            (1 - f) * Math.sqrt(sinAlpha * sinAlpha + tmp * tmp));
        let lambda = Math.atan2(sinSigma * sinAlpha1, cosU1 * cosSigma - sinU1 * sinSigma * cosAlpha1);
        let C = f / 16 * cosSqAlpha * (4 + f * (4 - 3 * cosSqAlpha));
        let L = lambda - (1 - C) * f * sinAlpha *
            (sigma + C * sinSigma * (cos2SigmaM + C * cosSigma * (-1 + 2 * cos2SigmaM * cos2SigmaM)));

        // let revAz = Math.atan2(sinAlpha, -tmp);
        return new Point(lon1 + this.radToDeg(L), this.radToDeg(lat2));
    };

}