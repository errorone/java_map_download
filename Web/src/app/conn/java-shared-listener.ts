import { Injectable } from '@angular/core';
import { MapBase } from '../map/map-base';
import { SharedService } from '../rx/shared.service';
import { getSharedMapFunction } from './shared-map-func';

declare var sharedService: any;

@Injectable()
export class JavaSharedListener {

    constructor(private shared: SharedService) {
        sharedService = shared;
    }

    public addSubMapSharedNoMap(topic: string, callback?: Function) {
        this.shared.sub(topic).subscribe((res: string) => {
            getSharedMapFunction(topic)(callback);
        });
    }

    public addSubMapShared(topic: string, mapBase: MapBase, callback?: Function) {
        this.shared.sub(topic).subscribe((res: string) => {
            getSharedMapFunction(topic)(mapBase, res, callback);
        });
    }

}