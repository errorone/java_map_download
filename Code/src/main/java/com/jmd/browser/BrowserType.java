package com.jmd.browser;

public enum BrowserType {

	JAVA_FX_BROWSER,
	TEAMDEV_JX_BROWSER,
	CHROMIUM_EMBEDDED_CEF_BROWSER;

}
