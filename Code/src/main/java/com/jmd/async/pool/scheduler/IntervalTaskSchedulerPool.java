package com.jmd.async.pool.scheduler;

import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.scheduling.support.PeriodicTrigger;
import org.springframework.stereotype.Component;

@Component
public class IntervalTaskSchedulerPool {

	@Lazy
	@Autowired
	private ThreadPoolTaskScheduler taskScheduler;

	@Bean
	private ThreadPoolTaskScheduler taskScheduler() {
		ThreadPoolTaskScheduler scheduler = new ThreadPoolTaskScheduler();
		// 定义一个线程池大小
		scheduler.setPoolSize(10);
		// 线程池名的前缀
		scheduler.setThreadNamePrefix("IntervalTaskSchedulerPool-");
		// 设置线程池关闭的时候等待所有任务都完成再继续销毁其他的Bean
		scheduler.setWaitForTasksToCompleteOnShutdown(true);
		// 设置线程池中任务的等待时间，如果超过这个时候还没有销毁就强制销毁，以确保应用最后能够被关闭，而不是阻塞住
		scheduler.setAwaitTerminationSeconds(60);
		// 线程池对拒绝任务的处理策略,当线程池没有处理能力的时候，该策略会直接在 execute
		// 方法的调用线程中运行被拒绝的任务；如果执行程序已关闭，则会丢弃该任务
		scheduler.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
		return scheduler;
	}

	public ScheduledFuture<?> setInterval(Runnable task, int secInterval) {
		ScheduledFuture<?> future = taskScheduler.schedule(task, (arg0) -> {
			String corn = "*/" + secInterval + " * * * * ?";
			return new CronTrigger(corn).nextExecutionTime(arg0);
		});
		return future;
	}

	public ScheduledFuture<?> setInterval(Runnable task, long millInterval) {
		PeriodicTrigger periodicTrigger = new PeriodicTrigger(millInterval, TimeUnit.MILLISECONDS);
		periodicTrigger.setFixedRate(true);
		periodicTrigger.setInitialDelay(millInterval);
		ScheduledFuture<?> future = taskScheduler.schedule(task, periodicTrigger);
		return future;
	}

	public boolean clearInterval(ScheduledFuture<?> future) {
		return future.cancel(true);
	}

}
