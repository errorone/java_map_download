package com.jmd.ui.tab.a_mapview.sub;

import javax.swing.JPanel;
import javax.annotation.PostConstruct;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.TitledBorder;

import com.jmd.taskfunc.TaskState;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.jmd.browser.BrowserEngine;
import com.jmd.common.StaticVar;
import com.jmd.taskfunc.TaskExecFunc;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.Serial;

@Component
public class TopToolPanel extends JPanel {

	@Serial
	private static final long serialVersionUID = 3596419206330955988L;

	@Autowired
	private TaskExecFunc taskExec;
	@Autowired
	private BrowserEngine browserEngine;

//	private ImageIcon getButtonImage(String name) {
//		ImageIcon imageIcon = new ImageIcon(TopToolPanel.class.getResource("/com/jmd/assets/btn/" + name));
//		imageIcon.setImage(imageIcon.getImage().getScaledInstance(20, 20, Image.SCALE_SMOOTH));
//		return imageIcon;
//	}

//	public TopToolPanel() {
//		init();
//	}

	@PostConstruct
	private void init() {

		this.setBorder(new TitledBorder(null, "操作", TitledBorder.LEADING, TitledBorder.TOP,
				StaticVar.FONT_SourceHanSansCNNormal_12, null));

		// zoom +
		JButton zoomInButton = new JButton("\uea0a");
		zoomInButton.setFont(StaticVar.ICON_FONT_ICOMOON_18);
		zoomInButton.setFocusable(false);
		zoomInButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				browserEngine.sendShared("ZoomIn", null);
			}
		});

		// zoom -
		JButton zoomOutButton = new JButton("\uea0b");
		zoomOutButton.setFont(StaticVar.ICON_FONT_ICOMOON_18);
		zoomOutButton.setFocusable(false);
		zoomOutButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				browserEngine.sendShared("ZoomOut", null);
			}
		});

		// 网格
		JButton gridButton = new JButton("\uea71");
		gridButton.setFont(StaticVar.ICON_FONT_ICOMOON_18);
		gridButton.setFocusable(false);
		gridButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				browserEngine.sendShared("GridSwitch", null);
			}
		});

		// 下载
		JButton downloadButton = new JButton("\ue9c7");
		downloadButton.setFont(StaticVar.ICON_FONT_ICOMOON_18);
		downloadButton.setFocusable(false);
		downloadButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				if (TaskState.IS_TASKING) {
					JOptionPane.showMessageDialog(null, "当前正在进行下载任务");
					return;
				}
				browserEngine.sendShared("SubmitBlockDownload", null);
			}
		});

		// 拖动
		JButton panButton = new JButton("\uea03");
		panButton.setFont(StaticVar.ICON_FONT_ICOMOON_18);
		panButton.setFocusable(false);
		panButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				browserEngine.sendShared("Pan", null);
			}
		});

		// 绘制
		JButton drawButton = new JButton("\ue905");
		drawButton.setFont(StaticVar.ICON_FONT_ICOMOON_18);
		drawButton.setFocusable(false);
		drawButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				browserEngine.sendShared("openDraw", null);
			}
		});

		// fitview
		JButton fitviewButton = new JButton("\ue989");
		fitviewButton.setFont(StaticVar.ICON_FONT_ICOMOON_18);
		fitviewButton.setFocusable(false);
		fitviewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				browserEngine.sendShared("fitview", null);
			}
		});

		// 移除
		JButton removeButton = new JButton("\ue9ac");
		removeButton.setFont(StaticVar.ICON_FONT_ICOMOON_18);
		removeButton.setFocusable(false);
		removeButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				browserEngine.sendShared("removeDrawedShape", null);
			}
		});

		GroupLayout gl_panel = new GroupLayout(this);
		gl_panel.setHorizontalGroup(gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup().addContainerGap()
						.addComponent(zoomInButton, GroupLayout.PREFERRED_SIZE, 38, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(zoomOutButton, GroupLayout.PREFERRED_SIZE, 38, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(gridButton, GroupLayout.PREFERRED_SIZE, 38, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(downloadButton, GroupLayout.PREFERRED_SIZE, 38, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(panButton, GroupLayout.PREFERRED_SIZE, 38, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(drawButton, GroupLayout.PREFERRED_SIZE, 38, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(fitviewButton, GroupLayout.PREFERRED_SIZE, 38, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(removeButton, GroupLayout.PREFERRED_SIZE, 38, GroupLayout.PREFERRED_SIZE)
						.addGap(12)));
		gl_panel.setVerticalGroup(gl_panel.createParallelGroup(Alignment.LEADING).addGroup(gl_panel
				.createSequentialGroup()
				.addGroup(gl_panel.createParallelGroup(Alignment.TRAILING, false)
						.addComponent(zoomInButton, Alignment.LEADING, GroupLayout.DEFAULT_SIZE,
								GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(zoomOutButton, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 38, Short.MAX_VALUE)
						.addComponent(gridButton, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 38, Short.MAX_VALUE)
						.addComponent(downloadButton, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 38, Short.MAX_VALUE)
						.addComponent(panButton, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 38, Short.MAX_VALUE)
						.addComponent(drawButton, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 38, Short.MAX_VALUE)
						.addComponent(fitviewButton, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 38, Short.MAX_VALUE)
						.addComponent(removeButton, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 38, Short.MAX_VALUE))
				.addContainerGap(6, Short.MAX_VALUE)));
		this.setLayout(gl_panel);

	}

}
