/** SharedService */
let sharedService = new SharedService();
/** 读取配置文件 */
let config = getConfig();
let drawType = "Polygon";

/** 实例化地图 */
let mapInst = new MapInst({
    dom: "map",
    layer: config.layer,
    grid: config.grid
});
setMaxZoom(config.layer);

// sharedService.pub("switchResource", "Google");
// sharedService.pub("GridSwitch", "");
// sharedService.pub("openDraw", "");

/** Zoom + */
sharedService.sub("ZoomIn").subscribe(() => { mapInst.zoomIn(); });
/** Zoom - */
sharedService.sub("ZoomOut").subscribe(() => { mapInst.zoomOut(); });
/** Pan - */
sharedService.sub("Pan").subscribe(() => { mapInst.pan(); });
/** 显示网格 */
sharedService.sub("GridSwitch").subscribe(() => {
    let update;
    if (mapInst.getGridVisible()) {
        mapInst.closeGrid();
        update = false;
    } else {
        mapInst.showGrid();
        update = true;;
    }
    saveConfig({ grid: update });
});
/** 切换图层源 */
sharedService.sub("switchResource").subscribe((res) => {
    let lastType = getCoordinateType();
    setMaxZoom(res);
    mapInst.switchMapResource(res);
    saveConfig({ layer: res });
    let currentType = getCoordinateType();
    if (lastType == currentType) {
        return;
    }
    if (lastType == "wgs84" && currentType == "gcj02") {
        turnMapFeaturesFromWgs84ToGcj02();
    } else if (lastType == "gcj02" && currentType == "wgs84") {
        turnMapFeaturesFromGcj02ToWgs84();
    }
});
/** 绘制类型切换 */
sharedService.sub("switchDrawType").subscribe((res) => {
    drawType = res;
})
/** 绘制 */
sharedService.sub("openDraw").subscribe((res) => {
    mapInst.removeDrawedFeatures();
    switch (drawType) {
        case "Polygon":
            mapInst.drawPolygon({
                drawEnd: () => {
                    setTimeout(() => {
                        mapInst.removeDrawInteraction();
                    })
                },
                modifyEnd: () => {
                }
            });
            break;
        case "Circle":
            mapInst.drawCircle({
                drawEnd: () => {
                    setTimeout(() => {
                        mapInst.removeDrawInteraction();
                    })
                },
                modifyEnd: () => {
                }
            });
            break;
        default:
            break;
    }
});
/** 绘制指定多边形并定位 */
sharedService.sub("drawPolygonAndPositioning").subscribe((res) => {
    mapInst.pan();
    mapInst.removeDrawedFeatures();
    let blocks = JSON.parse(res);
    for (let i = 0; i < blocks.length; i++) {
        mapInst.createPolygonFeature(blocks[i]);
    }
    mapInst.setFitview();
});
/** fitview */
sharedService.sub("fitview").subscribe((res) => {
    mapInst.setFitview();
});
/** 删除绘制 */
sharedService.sub("removeDrawedShape").subscribe((res) => {
    mapInst.removeDrawedFeatures();
});

function turnMapFeaturesFromWgs84ToGcj02() {
    let features = mapInst.getLayerByName("Draw").getSource().getFeatures();
    if (features != null && features.length > 0) {
        for (let i = 0; i < features.length; i++) {
            let feature = features[i];
            let shapes = feature.getGeometry();
            switch (shapes.constructor) {
                case ol.geom.Polygon: {
                    let polygonPoints = feature.getGeometry().getCoordinates()[0];
                    let wgs84Lnglat = [];
                    for (let j = 0; j < polygonPoints.length; j++) {
                        wgs84Lnglat.push(mercator_To_LngLat({ lng: polygonPoints[j][0], lat: polygonPoints[j][1] }));
                    }
                    let gcj02Lnglat = [];
                    for (let j = 0; j < wgs84Lnglat.length; j++) {
                        gcj02Lnglat.push(wgs84_To_gcj02({ lng: wgs84Lnglat[j].lng, lat: wgs84Lnglat[j].lat }));
                    }
                    let pts = [];
                    for (let j = 0; j < gcj02Lnglat.length; j++) {
                        pts.push(ol.proj.fromLonLat([gcj02Lnglat[j].lng, gcj02Lnglat[j].lat]));
                    }
                    feature.getGeometry().setCoordinates([pts]);
                    break;
                }
                case ol.geom.Circle: {
                    let center = feature.getGeometry().getCenter();
                    let wgs84Lnglat = mercator_To_LngLat({ lng: center[0], lat: center[1] });
                    let gcj02Lnglat = wgs84_To_gcj02({ lng: wgs84Lnglat.lng, lat: wgs84Lnglat.lat });
                    let pt = ol.proj.fromLonLat([gcj02Lnglat.lng, gcj02Lnglat.lat]);
                    feature.getGeometry().setCenter(pt);
                    break;
                }
                default:
                    break;
            }
        }
    }
}

function turnMapFeaturesFromGcj02ToWgs84() {
    let features = mapInst.getLayerByName("Draw").getSource().getFeatures();
    if (features != null && features.length > 0) {
        for (let i = 0; i < features.length; i++) {
            let feature = features[i];
            let shapes = feature.getGeometry();
            switch (shapes.constructor) {
                case ol.geom.Polygon: {
                    let polygonPoints = feature.getGeometry().getCoordinates()[0];
                    let gcj02Lnglat = [];
                    for (let j = 0; j < polygonPoints.length; j++) {
                        gcj02Lnglat.push(mercator_To_LngLat({ lng: polygonPoints[j][0], lat: polygonPoints[j][1] }));
                    }
                    let wgs84Lnglat = [];
                    for (let j = 0; j < gcj02Lnglat.length; j++) {
                        wgs84Lnglat.push(gcj02_To_wgs84({ lng: gcj02Lnglat[j].lng, lat: gcj02Lnglat[j].lat }));
                    }
                    let pts = [];
                    for (let j = 0; j < wgs84Lnglat.length; j++) {
                        pts.push(ol.proj.fromLonLat([wgs84Lnglat[j].lng, wgs84Lnglat[j].lat]));
                    }
                    feature.getGeometry().setCoordinates([pts]);
                    break;
                }
                case ol.geom.Circle: {
                    let center = feature.getGeometry().getCenter();
                    let gcj02Lnglat = mercator_To_LngLat({ lng: center[0], lat: center[1] });
                    let wgs84Lnglat = gcj02_To_wgs84({ lng: gcj02Lnglat.lng, lat: gcj02Lnglat.lat });
                    let pt = ol.proj.fromLonLat([wgs84Lnglat.lng, wgs84Lnglat.lat]);
                    feature.getGeometry().setCenter(pt);
                    break;
                }
                default:
                    break;
            }
        }
    }
}

function getDrawedPoints() {
    let points = [];
    let features = mapInst.getLayerByName("Draw").getSource().getFeatures();
    if (features != null && features.length > 0) {
        for (let i = 0; i < features.length; i++) {
            let feature = features[i];
            let shapes = feature.getGeometry();
            switch (shapes.constructor) {
                case ol.geom.Polygon:
                    let polygonPoints = feature.getGeometry().getCoordinates()[0];
                    if (polygonPoints != null && polygonPoints.length > 3) {
                        points.push(polygonPoints);
                    }
                    break;
                case ol.geom.Circle:
                    let center = feature.getGeometry().getCenter();
                    let radius = feature.getGeometry().getRadius();
                    let circlePoints = getCirclePoints(center, radius)
                    points.push(circlePoints);
                    break;
                default:
                    break;
            }
        }
    }
    return JSON.stringify(points);
}

function getCoordinateType() {
    return mapInst.getCurrentCoordinateType();
}

function getTileUrl() {
    return JSON.stringify(mapInst.getCurrentXyzUrlResources());
}

function getMapType() {
    let name = mapInst.getCurrentXyzName();
    switch (name) {
        case "OpenStreet":
            return "OpenStreet";
        case "Google-Normal":
            return "谷歌地图（普通带标注）";
        case "Google-Terrain":
            return "谷歌地图（地形带标注）";
        case "Google-Satellite":
            return "谷歌地图（影像带标注）";
        case "Google-Satellite-None":
            return "谷歌地图（影像无标注）";
        case "Google-Street":
            return "谷歌地图（路网带标注）";
        case "AMap-Normal":
            return "高德地图（普通带标注）";
        case "AMap-Normal-None":
            return "高德地图（普通无标注）";
        case "AMap-Satellite-None":
            return "高德地图（影像无标注）";
        case "AMap-Street":
            return "高德地图（路网带标注）";
        case "AMap-Street-None":
            return "高德地图（路网无标注）";
        case "Tianditu-Normal-None":
            return "天地图（普通无标注）";
        case "Tianditu-Terrain-None":
            return "天地图（地形无标注）";
        case "Tianditu-Line":
            return "天地图（边界无标注）";
        case "Tianditu-Tip":
            return "天地图（标注层）";
        default:
            break;
    }
}

function setMaxZoom(layerName) {
    switch (layerName) {
        case "OpenStreet":
            mapInst.setMaxZoom(19);
            break;
        case "Google":
            mapInst.setMaxZoom(19);
            break;
        case "AMap":
            mapInst.setMaxZoom(18);
            break;
        case "Tianditu":
            mapInst.setMaxZoom(18);
            break;
        default:
            break;
    }
}

function getConfig() {
    let json, config;
    json = localStorage.getItem("jmd-config");
    if (json && json != "") {
        config = JSON.parse(json);
    } else {
        config = {
            layer: "OpenStreet",
            grid: false,
        }
    }
    return config;
}

function saveConfig(update) {
    for (let key in update) {
        config[key] = update[key];
    }
    localStorage.setItem("jmd-config", JSON.stringify(config));
}