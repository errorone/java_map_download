const EARTH_RADIUS = 6370996.81;
const aaa = 6378245.0;
const ee = 0.00669342162296594323;

function getCirclePoints(center, radius) {
    let _center = mercator_To_LngLat({ lng: center[0], lat: center[1] });
    let num = 72;
    let cphase = 2 * Math.PI / num;
    let pts = [];
    for (let k = 0; k < num; k++) {
        let dx = (radius * Math.cos(k * cphase));
        let dy = (radius * Math.sin(k * cphase));
        let dlng = dx / (EARTH_RADIUS * Math.cos(_center.lat * Math.PI / 180) * Math.PI / 180);
        let dlat = dy / (EARTH_RADIUS * Math.PI / 180);
        let newlng = _center.lng + dlng;
        let newlat = _center.lat + dlat;
        let pt = lngLat_To_Mercator({ lng: newlng, lat: newlat });
        pts.push([pt.lng, pt.lat]);
    }
    return pts;
}

function lngLat_To_Mercator(point) {
    let x = point.lng * 20037508.34 / 180;
    let y = Math.log(Math.tan((90 + point.lat) * Math.PI / 360)) / (Math.PI / 180);
    y = y * 20037508.34 / 180;
    return { lng: x, lat: y };
}

function mercator_To_LngLat(point) {
    let x = point.lng / 20037508.34 * 180;
    let y = point.lat / 20037508.34 * 180;
    y = 180 / Math.PI * (2 * Math.atan(Math.exp(y * Math.PI / 180)) - Math.PI / 2);
    return { lng: x, lat: y };
}

function wgs84_To_gcj02(point) {
    let lat = +point.lat;
    let lng = +point.lng;
    if (outOfChina(point)) {
        return point;
    } else {
        let dlat = transformLat({ lng: lng - 105.0, lat: lat - 35.0 });
        let dlng = transformLng({ lng: lng - 105.0, lat: lat - 35.0 });
        let radlat = lat / 180.0 * Math.PI;
        let magic = Math.sin(radlat);
        magic = 1 - ee * magic * magic;
        let sqrtmagic = Math.sqrt(magic);
        dlat = (dlat * 180.0) / ((aaa * (1 - ee)) / (magic * sqrtmagic) * Math.PI);
        dlng = (dlng * 180.0) / (aaa / sqrtmagic * Math.cos(radlat) * Math.PI);
        let mglat = lat + dlat;
        let mglng = lng + dlng;
        return { lng: mglng, lat: mglat };
    }
}

function gcj02_To_wgs84(point) {
    let lat = +point.lat;
    let lng = +point.lng;
    if (outOfChina(point)) {
        return point;
    } else {
        let dlat = transformLat({ lng: lng - 105.0, lat: lat - 35.0 });
        let dlng = transformLng({ lng: lng - 105.0, lat: lat - 35.0 });
        let radlat = lat / 180.0 * Math.PI;
        let magic = Math.sin(radlat);
        magic = 1 - ee * magic * magic;
        let sqrtmagic = Math.sqrt(magic);
        dlat = (dlat * 180.0) / ((aaa * (1 - ee)) / (magic * sqrtmagic) * Math.PI);
        dlng = (dlng * 180.0) / (aaa / sqrtmagic * Math.cos(radlat) * Math.PI);
        let mglat = lat + dlat;
        let mglng = lng + dlng;
        return { lng: lng * 2 - mglng, lat: lat * 2 - mglat };
    }
}

function outOfChina(point) {
    let lat = +point.lat;
    let lng = +point.lng;
    // 纬度3.86~53.55,经度73.66~135.05
    return !(lng > 73.66 && lng < 135.05 && lat > 3.86 && lat < 53.55);
}

function transformLng(point) {
    let lat = +point.lat;
    let lng = +point.lng;
    let ret = 300.0 + lng + 2.0 * lat + 0.1 * lng * lng + 0.1 * lng * lat + 0.1 * Math.sqrt(Math.abs(lng));
    ret += (20.0 * Math.sin(6.0 * lng * Math.PI) + 20.0 * Math.sin(2.0 * lng * Math.PI)) * 2.0 / 3.0;
    ret += (20.0 * Math.sin(lng * Math.PI) + 40.0 * Math.sin(lng / 3.0 * Math.PI)) * 2.0 / 3.0;
    ret += (150.0 * Math.sin(lng / 12.0 * Math.PI) + 300.0 * Math.sin(lng / 30.0 * Math.PI)) * 2.0 / 3.0;
    return ret;
}

function transformLat(point) {
    let lat = +point.lat;
    let lng = +point.lng;
    let ret = -100.0 + 2.0 * lng + 3.0 * lat + 0.2 * lat * lat + 0.1 * lng * lat + 0.2 * Math.sqrt(Math.abs(lng));
    ret += (20.0 * Math.sin(6.0 * lng * Math.PI) + 20.0 * Math.sin(2.0 * lng * Math.PI)) * 2.0 / 3.0;
    ret += (20.0 * Math.sin(lat * Math.PI) + 40.0 * Math.sin(lat / 3.0 * Math.PI)) * 2.0 / 3.0;
    ret += (160.0 * Math.sin(lat / 12.0 * Math.PI) + 320 * Math.sin(lat * Math.PI / 30.0)) * 2.0 / 3.0;
    return ret;
}

/** setFitViewByFeatures */
function setFitViewByFeatures(data) {
    if (data.features == null || data.features.length == 0) {
        return;
    }
    let shapes = [];
    for (let i = 0; i < data.features.length; i++) {
        shapes.push(data.features[i].getGeometry());
    }
    this.setFitViewByShapes({
        map: data.map,
        shapes: shapes,
        padding: data.padding
    })
}

/** setFitViewByShapes */
function setFitViewByShapes(data) {
    if (data.shapes == null || data.shapes.length == 0) {
        return;
    }
    let points = {
        minS: [],
        maxS: [],
    };
    for (let i = 0; i < data.shapes.length; i++) {
        switch (data.shapes[i].constructor) {
            case ol.geom.LineString: {
                let shape = data.shapes[i];
                let coord = shape.getCoordinates();
                let minX, minY, maxX, maxY;
                for (let n = 0; n < coord.length; n++) {
                    minX = minX ? (minX > coord[n][0] ? coord[n][0] : minX) : coord[n][0];
                    minY = minY ? (minY > coord[n][1] ? coord[n][1] : minY) : coord[n][1];
                    maxX = maxX ? (maxX < coord[n][0] ? coord[n][0] : maxX) : coord[n][0];
                    maxY = maxY ? (maxY < coord[n][1] ? coord[n][1] : maxY) : coord[n][1];
                }
                points.minS.push({ lng: minX, lat: minY });
                points.maxS.push({ lng: maxX, lat: maxY });
                break;
            }
            case ol.geom.Polygon: {
                let shape = data.shapes[i];
                let coords = shape.getCoordinates();
                for (let h = 0; h < coords.length; h++) {
                    let coord = coords[h];
                    let minX, minY, maxX, maxY;
                    for (let n = 0; n < coord.length; n++) {
                        minX = minX ? (minX > coord[n][0] ? coord[n][0] : minX) : coord[n][0];
                        minY = minY ? (minY > coord[n][1] ? coord[n][1] : minY) : coord[n][1];
                        maxX = maxX ? (maxX < coord[n][0] ? coord[n][0] : maxX) : coord[n][0];
                        maxY = maxY ? (maxY < coord[n][1] ? coord[n][1] : maxY) : coord[n][1];
                    }
                    points.minS.push({ lng: minX, lat: minY });
                    points.maxS.push({ lng: maxX, lat: maxY });
                }
                break;
            }
            case ol.geom.Circle: {
                let shape = data.shapes[i];
                let m_center = shape.getCenter();
                let center = mercator_To_LngLat({ lng: m_center[0], lat: m_center[1] });
                let radius = shape.getRadius();
                let distance = Math.sqrt(2 * Math.pow(radius, 2));
                let bottomLeft = destinationVincenty(center, 225, distance);
                let topRight = destinationVincenty(center, 45, distance);
                points.minS.push(lngLat_To_Mercator(bottomLeft));
                points.maxS.push(lngLat_To_Mercator(topRight));
                break;
            }
            default:
                break;
        }
    }
    let minX, minY, maxX, maxY;
    for (let i = 0; i < points.minS.length; i++) {
        minX = minX ? (minX > points.minS[i].lng ? points.minS[i].lng : minX) : points.minS[i].lng;
        minY = minY ? (minY > points.minS[i].lat ? points.minS[i].lat : minY) : points.minS[i].lat;
    }
    for (let i = 0; i < points.maxS.length; i++) {
        maxX = maxX ? (maxX < points.maxS[i].lng ? points.maxS[i].lng : maxX) : points.maxS[i].lng;
        maxY = maxY ? (maxY < points.maxS[i].lat ? points.maxS[i].lat : maxY) : points.maxS[i].lat;
    }
    let ext = [minX, minY, maxX, maxY];
    let view = data.map.getView();
    view.fit(ext, {
        nearest: false,
        padding: data.padding
    })
}

/** setFitView */
function setFitView(map, extent) {
    let view = map.getView();
    let m_bottomLeft = lngLat_To_Mercator(extent.bottomLeft);
    let m_topRight = lngLat_To_Mercator(extent.topRight);
    let ext = [m_bottomLeft.lng, m_bottomLeft.lat, m_topRight.lng, m_topRight.lat];
    view.fit(ext, {
        nearest: false
    })
}

/** 将度转化为弧度 */
function degToRad(deg) {
    return Math.PI * deg / 180;
}

/** 将弧度转化为度 */
function radToDeg(rad) {
    return rad * 180 / Math.PI;
}

/** 根据某一点与北向夹角和距离，求另一点 */
function destinationVincenty(point, northAngle, distance) {
    /* 椭圆球基础参数*/
    let VincentyConstants = {
        a: aaa,
        b: 6356752.3142,
        f: 1 / 298.257223563
    };
    let ct = VincentyConstants;
    let a = ct.a, b = ct.b, f = ct.f;

    let lon1 = point.lng;
    let lat1 = point.lat;

    let s = distance;
    let alpha1 = this.degToRad(northAngle);
    let sinAlpha1 = Math.sin(alpha1);
    let cosAlpha1 = Math.cos(alpha1);

    let tanU1 = (1 - f) * Math.tan(this.degToRad(lat1));
    let cosU1 = 1 / Math.sqrt((1 + tanU1 * tanU1)), sinU1 = tanU1 * cosU1;
    let sigma1 = Math.atan2(tanU1, cosAlpha1);
    let sinAlpha = cosU1 * sinAlpha1;
    let cosSqAlpha = 1 - sinAlpha * sinAlpha;
    let uSq = cosSqAlpha * (a * a - b * b) / (b * b);
    let A = 1 + uSq / 16384 * (4096 + uSq * (-768 + uSq * (320 - 175 * uSq)));
    let B = uSq / 1024 * (256 + uSq * (-128 + uSq * (74 - 47 * uSq)));

    let sigma = s / (b * A), sigmaP = 2 * Math.PI;
    let cos2SigmaM, sinSigma, cosSigma;
    while (Math.abs(sigma - sigmaP) > 1e-12) {
        cos2SigmaM = Math.cos(2 * sigma1 + sigma);
        sinSigma = Math.sin(sigma);
        cosSigma = Math.cos(sigma);
        let deltaSigma = B * sinSigma * (cos2SigmaM + B / 4 * (cosSigma * (-1 + 2 * cos2SigmaM * cos2SigmaM) -
            B / 6 * cos2SigmaM * (-3 + 4 * sinSigma * sinSigma) * (-3 + 4 * cos2SigmaM * cos2SigmaM)));
        sigmaP = sigma;
        sigma = s / (b * A) + deltaSigma;
    }

    let tmp = sinU1 * sinSigma - cosU1 * cosSigma * cosAlpha1;
    let lat2 = Math.atan2(sinU1 * cosSigma + cosU1 * sinSigma * cosAlpha1,
        (1 - f) * Math.sqrt(sinAlpha * sinAlpha + tmp * tmp));
    let lambda = Math.atan2(sinSigma * sinAlpha1, cosU1 * cosSigma - sinU1 * sinSigma * cosAlpha1);
    let C = f / 16 * cosSqAlpha * (4 + f * (4 - 3 * cosSqAlpha));
    let L = lambda - (1 - C) * f * sinAlpha *
        (sigma + C * sinSigma * (cos2SigmaM + C * cosSigma * (-1 + 2 * cos2SigmaM * cos2SigmaM)));

    // let revAz = Math.atan2(sinAlpha, -tmp);
    return { lng: lon1 + this.radToDeg(L), lat: this.radToDeg(lat2) };
};