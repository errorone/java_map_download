/** 地图图层声明 */
let layerResources = new Map();

/** -----OpenStreet----- */
let OpenStreet_URL = "https://{a-c}.tile.openstreetmap.org/{z}/{x}/{y}.png";
layerResources.set("OpenStreet", [
    { // OpenStreet
        name: "Normal", type: "XYZ_URL", support: true,
        source: new ol.source.XYZ({ url: OpenStreet_URL }),
        url: OpenStreet_URL,
        coordinateType: "wgs84"
    }
]);

/** -----谷歌地图----- */
let GoogleNormal_URL = "https://mt{1-3}.google.cn/maps/vt?lyrs=m%40781&hl=zh-CN&gl=CN&x={x}&y={y}&z={z}";
layerResources.set("Google-Normal", [
    { // 谷歌地图-普通图-带标注
        name: "Normal", type: "XYZ_URL", support: true,
        source: new ol.source.XYZ({ url: GoogleNormal_URL }),
        url: GoogleNormal_URL,
        coordinateType: "gcj02"
    },
]);
let GoogleTerrain_URL = "https://mt{1-3}.google.cn/maps/vt?lyrs=p%40781&hl=zh-CN&gl=CN&x={x}&y={y}&z={z}";
layerResources.set("Google-Terrain", [
    { // 谷歌地图-地形图-带标注
        name: "Terrain", type: "XYZ_URL", support: true,
        source: new ol.source.XYZ({ url: GoogleTerrain_URL }),
        url: GoogleTerrain_URL,
        coordinateType: "gcj02"
    },
]);
let GoogleSatellite_URL = "https://mt{1-3}.google.cn/maps/vt?lyrs=y%40781&hl=zh-CN&gl=CN&x={x}&y={y}&z={z}";
layerResources.set("Google-Satellite", [
    { // 谷歌地图-影像图-带标注
        name: "Satellite", type: "XYZ_URL", support: true,
        source: new ol.source.XYZ({ url: GoogleSatellite_URL }),
        url: GoogleSatellite_URL,
        coordinateType: "gcj02"
    }
]);
let GoogleSatelliteNone_URL = "https://mt{1-3}.google.cn/maps/vt?lyrs=s%40781&hl=zh-CN&gl=CN&x={x}&y={y}&z={z}";
layerResources.set("Google-Satellite-None", [
    { // 谷歌地图-影像图-无标注
        name: "Satellite-None", type: "XYZ_URL", support: true,
        source: new ol.source.XYZ({ url: GoogleSatelliteNone_URL }),
        url: GoogleSatelliteNone_URL,
        coordinateType: "gcj02"
    },
]);
let GoogleStreet_URL = "https://mt{1-3}.google.cn/maps/vt?lyrs=h%40781&hl=zh-CN&gl=CN&x={x}&y={y}&z={z}";
layerResources.set("Google-Street", [
    { // 谷歌地图-路网图-带标注
        name: "Street", type: "XYZ_URL", support: true,
        source: new ol.source.XYZ({ url: GoogleStreet_URL }),
        url: GoogleStreet_URL,
        coordinateType: "gcj02"
    },
]);

/** -----高德地图----- */
let AMapNormal_URL = "https://webrd0{1-4}.is.autonavi.com/appmaptile?x={x}&y={y}&z={z}&lang=zh_cn&size=1&scl=1&style=8";
layerResources.set("AMap-Normal", [
    { // 高德地图-普通图-带标注
        name: "Normal", type: "XYZ_URL", support: true,
        source: new ol.source.XYZ({ url: AMapNormal_URL }),
        url: AMapNormal_URL,
        coordinateType: "gcj02"
    },
]);
let AMapNormalNone_URL = "https://webrd0{1-4}.is.autonavi.com/appmaptile?x={x}&y={y}&z={z}&lang=zh_cn&size=1&scl=1&style=8&ltype=11";
layerResources.set("AMap-Normal-None", [
    { // 高德地图-普通图-无标注
        name: "Normal-None", type: "XYZ_URL", support: true,
        source: new ol.source.XYZ({ url: AMapNormalNone_URL }),
        url: AMapNormalNone_URL,
        coordinateType: "gcj02"
    },
]);
let AMapSatelliteNone_URL = "https://webst0{1-4}.is.autonavi.com/appmaptile?x={x}&y={y}&z={z}&lang=zh_cn&size=1&scl=1&style=6";
layerResources.set("AMap-Satellite-None", [
    { // 高德地图-影像图-无标注
        name: "Satellite-None", type: "XYZ_URL", support: true,
        source: new ol.source.XYZ({ url: AMapSatelliteNone_URL }),
        url: AMapSatelliteNone_URL,
        coordinateType: "gcj02"
    },
]);
let AMapStreet_URL = "https://webst0{1-4}.is.autonavi.com/appmaptile?x={x}&y={y}&z={z}&lang=zh_cn&size=1&scl=1&style=8";
layerResources.set("AMap-Street", [
    { // 高德地图-路网图-带标注
        name: "Street", type: "XYZ_URL", support: true,
        source: new ol.source.XYZ({ url: AMapStreet_URL }),
        url: AMapStreet_URL,
        coordinateType: "gcj02"
    },
]);
let AMapStreetNone_URL = "https://webst0{1-4}.is.autonavi.com/appmaptile?x={x}&y={y}&z={z}&lang=zh_cn&size=1&scl=1&style=8&ltype=11";
layerResources.set("AMap-Street-None", [
    { // 高德地图-路网图-无标注
        name: "Street-None", type: "XYZ_URL", support: true,
        source: new ol.source.XYZ({ url: AMapStreetNone_URL }),
        url: AMapStreetNone_URL,
        coordinateType: "gcj02"
    },
]);

/** -----腾讯地图----- */
let TencentNormal_URL = "http://rt0.map.gtimg.com/realtimerender?z={z}&x={x}&y={-y}&type=Normal&style=0";
layerResources.set("Tencent-Normal", [
    { // 腾讯地图-普通图-带标注
        name: "Normal", type: "XYZ_URL", support: true,
        source: new ol.source.XYZ({ url: TencentNormal_URL }),
        url: TencentNormal_URL
    },
]);

/** -----天地图----- */
let TiandituNormalNone_URL = "https://t{0-7}.tianditu.gov.cn/vec_w/wmts?SERVICE=WMTS&REQUEST=GetTile&VERSION=1.0.0&LAYER=vec&STYLE=default&TILEMATRIXSET=w&FORMAT=tiles&TILECOL={x}&TILEROW={y}&TILEMATRIX={z}&tk=81dd3e1715be9d296f1a9f2b0c0196aa";
layerResources.set("Tianditu-Normal-None", [
    { // 天地图-普通图-无标注
        name: "Normal-None", type: "XYZ_URL", support: true,
        source: new ol.source.XYZ({ url: TiandituNormalNone_URL }),
        url: TiandituNormalNone_URL,
        coordinateType: "wgs84"
    },
]);
let TiandituTerrainNone_URL = "https://t{0-7}.tianditu.gov.cn/ter_w/wmts?SERVICE=WMTS&REQUEST=GetTile&VERSION=1.0.0&LAYER=ter&STYLE=default&TILEMATRIXSET=w&FORMAT=tiles&TILECOL={x}&TILEROW={y}&TILEMATRIX={z}&tk=81dd3e1715be9d296f1a9f2b0c0196aa";
layerResources.set("Tianditu-Terrain-None", [
    { // 天地图-地形图-无标注
        name: "Terrain-None", type: "XYZ_URL", support: true,
        source: new ol.source.XYZ({ url: TiandituTerrainNone_URL }),
        url: TiandituTerrainNone_URL,
        coordinateType: "wgs84"
    },
]);
let TiandituLine_URL = "https://t{0-7}.tianditu.gov.cn/ibo_w/wmts?SERVICE=WMTS&REQUEST=GetTile&VERSION=1.0.0&LAYER=ibo&STYLE=default&TILEMATRIXSET=w&FORMAT=tiles&TILECOL={x}&TILEROW={y}&TILEMATRIX={z}&tk=81dd3e1715be9d296f1a9f2b0c0196aa";
layerResources.set("Tianditu-Line", [
    { // 天地图-边界线
        name: "Normal-Tip", type: "XYZ_URL", support: true,
        source: new ol.source.XYZ({ url: TiandituLine_URL }),
        url: TiandituLine_URL,
        coordinateType: "wgs84"
    },
]);
let TiandituTip_URL = "https://t{0-7}.tianditu.gov.cn/cva_w/wmts?SERVICE=WMTS&REQUEST=GetTile&VERSION=1.0.0&LAYER=cva&STYLE=default&TILEMATRIXSET=w&FORMAT=tiles&TILECOL={x}&TILEROW={y}&TILEMATRIX={z}&tk=81dd3e1715be9d296f1a9f2b0c0196aa";
layerResources.set("Tianditu-Tip", [
    { // 天地图-标注层
        name: "Normal-Tip", type: "XYZ_URL", support: true,
        source: new ol.source.XYZ({ url: TiandituTip_URL }),
        url: TiandituTip_URL,
        coordinateType: "wgs84"
    },
]);

/** -----百度地图----- */
let bdResolutions = [];
for (let i = 0; i < 19; i++) {
    bdResolutions[i] = Math.pow(2, 18 - i);
}
let bdTilegrid = new ol.tilegrid.TileGrid({
    origin: [0, 0],
    resolutions: bdResolutions
});
layerResources.set("Baidu-Normal", [
    { // 百度地图-普通图-带标注
        name: "Normal", type: "TILE_FUNC", support: false,
        source: new ol.source.TileImage({
            projection: 'EPSG:3857',
            tileGrid: bdTilegrid,
            tileUrlFunction: (tileCoord, pixelRatio, proj) => {
                if (!tileCoord) {
                    return "";
                }
                var z = tileCoord[0];
                var x = tileCoord[1];
                var y = -tileCoord[2] - 1; //y坐标变成相反数
                if (x < 0) {
                    x = "M" + (-x);
                }
                if (y < 0) {
                    y = "M" + (-y);
                }
                return "http://online1.map.bdimg.com/onlinelabel/?qt=tile&x=" + x + "&y=" + y + "&z=" + z + "&styles=pl&scaler=1&p=1"
            }
        }),
    },
]);